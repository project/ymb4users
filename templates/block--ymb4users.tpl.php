<?php

/**
 * @file
 * Template file.
 *
 * Available variables:
 *
 * $variables['description']
 * $variables['sum']
 * $variables['receiver']
 * $variables['target']
 * $variables['url']
 * $variables['delta']
 */
?>

<div class='ymb-form'>
  <p><?php print render($variables['description']); ?></p>
  <form method='POST' action='https://money.yandex.ru/quickpay/confirm.xml'>
    <div class='form-item form-type-textfield form-item-sum'>
      <label for='edit-sum--<?php print render($variables['delta']); ?>'><?php print t('Amount'); ?> <span class='form-required' title='<?php print t('This field is required.'); ?>'>*</span></label>
      <input id='edit-sum--<?php print render($variables['delta']); ?>' name='sum' value='<?php print render($variables['sum']); ?>' data-type='number' class='form-text required'>
    </div>
    <div class='form-item form-type-radios form-item-payment-type'>
      <div id='edit-payment-type--<?php print render($variables['delta']); ?>' class='form-radios'>
        <div class='form-item form-type-radio form-item-payment-type'>
          <input id='edit-payment-type-pc--<?php print render($variables['delta']); ?>' type='radio' name='paymentType' value='PC' class='form-radio' checked><label class='option' for='edit-payment-type-pc--<?php print render($variables['delta']); ?>'><?php print t('Yandex money'); ?></label>
        </div>
        <div class='form-item form-type-radio form-item-payment-type'>
          <input id='edit-payment-type-ac--<?php print render($variables['delta']); ?>' type='radio' name='paymentType' value='AC' class='form-radio'><label class='option' for='edit-payment-type-ac--<?php print render($variables['delta']); ?>'><?php print t('Bank card'); ?></label>
        </div>
      </div>
    </div>
    <div class='form-actions form-wrapper' id='edit-actions--<?php print render($variables['delta']); ?>'>
      <input id='edit-submit--<?php print render($variables['delta']); ?>' type='submit' value='<?php print t('Donate'); ?>' class='form-submit btn-primary btn'>
    </div>
    <input type='hidden' name='receiver' value='<?php print render($variables['receiver']); ?>'>
    <input type='hidden' name='targets' value='<?php print render($variables['target']); ?>'>
    <input type='hidden' name='quickpay-form' value='donate'>
    <input type='hidden' name='successURL' value='<?php print render($variables['url']); ?>'>
  </form>
</div>
